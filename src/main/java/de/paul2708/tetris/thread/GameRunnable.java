package de.paul2708.tetris.thread;

import de.paul2708.tetris.game.Game;
import de.paul2708.tetris.module.Direction;
import de.paul2708.tetris.module.Module;

/**
 * Created by Paul on 17.07.2017.
 */
public class GameRunnable implements Runnable {

    private Game game;

    public GameRunnable(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        while (true) {
            Module module = game.getCurrent();

            sleep(module.isStraight() ? 50 : 550);

            if (module.canMove(Direction.DOWN)) {
                module.move(Direction.DOWN);
            } else {
                // Check, if row is full
                int y = module.getLowest();

                for (int i = y; i >= 0; i--) {
                    while (game.fullLine(i)) {
                        game.destroyLine(i);
                        sleep(500);
                        game.compress(i);
                        sleep(500);
                    }
                }

                // Spawn new module
                game.spawn();
            }
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
