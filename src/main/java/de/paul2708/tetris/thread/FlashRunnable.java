package de.paul2708.tetris.thread;

import de.paul2708.tetris.game.Game;

/**
 * Created by Paul on 19.07.2017.
 */
public class FlashRunnable implements Runnable {

    private Game game;

    public FlashRunnable(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            game.getField().flash();
        }
    }
}
