package de.paul2708.tetris.game;

import de.paul2708.tetris.module.Block;
import de.paul2708.tetris.module.Direction;
import de.paul2708.tetris.module.Module;
import de.paul2708.tetris.module.structure.Structure;
import de.paul2708.tetris.sound.SoundManager;
import de.paul2708.tetris.state.PlayState;
import de.paul2708.tetris.thread.FlashRunnable;
import de.paul2708.tetris.thread.GameRunnable;
import de.paul2708.tetris.util.Constants;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 13.07.2017.
 */
public class Game extends StateBasedGame {

    private AppGameContainer container;

    private Field field;
    private SoundManager soundManager;

    private Module current;
    private Module next;
    private List<Module> modules;

    private Thread startThread;
    private Thread gameThread;

    private boolean start;

    public Game(String name) {
        super(name);

        initialize();
    }

    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        this.container = (AppGameContainer) gameContainer;

        getState(Constants.PLAY).init(container, this);

        enterState(Constants.PLAY);
    }

    private void initialize() {
        this.field = new Field(this);
        this.soundManager = new SoundManager();

        this.modules = new CopyOnWriteArrayList<>();

        this.startThread = new Thread(new FlashRunnable(this));
        startThread.start();
        this.gameThread = new Thread(new GameRunnable(this));

        this.start = false;

        addState(new PlayState(this));
    }

    public void start() {
        spawn();

        this.start = true;

        startThread.stop();
        gameThread.start();

        soundManager.playMusic();
    }

    public void spawn() {
        if (next == null) {
            this.current = new Module(modules.size(), Structure.random(), this);
        } else {
            this.current = next;
        }

        current.spawn();

        modules.add(current);

        this.next = new Module(modules.size(), Structure.random(), this);
    }

    public boolean fullLine(int y) {
        int count = 0;

        for (Module module : modules) {
            for (Block block : module.getBlocks()) {
                if (block.getY() == y) {
                    count++;
                }
            }
        }

        return count == 10;
    }

    public void destroyLine(int y) {
        soundManager.destroy();

        for (Module module : modules) {
            for (Block block : module.getBlocks()) {
                if (block.getY() == y) {
                    module.remove(block);
                }
            }

            if (module.empty()) {
                modules.remove(module);
            }
        }
    }

    public void compress(int y) {
        for (int i = 0; i < y; i++) {
            for (Module module : modules) {
                if (module.canMove(Direction.DOWN) && module.getLowest() <= y) {
                    module.move(Direction.DOWN);
                }
            }
        }
    }

    // Getter
    public Field getField() {
        return field;
    }

    public Module getCurrent() {
        return current;
    }

    public Module getNext() {
        return next;
    }

    public List<Module> getModules() {
        return modules;
    }

    public boolean hasStarted() {
        return start;
    }
}
