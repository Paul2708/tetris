package de.paul2708.tetris.game;

import de.paul2708.tetris.module.Block;
import de.paul2708.tetris.module.Module;
import de.paul2708.tetris.util.Constants;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

import java.awt.*;

/**
 * Created by Paul on 16.07.2017.
 */
public class Field {

    private Game game;

    private boolean flash;
    private boolean grid;

    private float fieldX;

    private Color backgroundColor;

    public Field(Game game) {
        this.game = game;

        this.flash = false;
        this.grid = false;

        this.backgroundColor = Color.blue;
    }

    public void calculate() {
        this.fieldX = (float) Constants.WIDTH - (5*32 * Constants.X_SCALE);
    }

    public void draw(Graphics graphic) {
        // Draw border
        graphic.setColor(backgroundColor);
        graphic.fillRect(0, 0, (float) Constants.WIDTH, (float) Constants.HEIGHT);

        graphic.setColor(Color.gray);
        graphic.fillRect(0, 0, 32 * Constants.X_SCALE, (float) Constants.HEIGHT);
        graphic.fillRect(fieldX, 0, 160 * Constants.X_SCALE, (float) Constants.HEIGHT); // 5*32

        // Draw field
        for (Module module : game.getModules()) {
            graphic.setColor(module.getColor().getColor());

            for (Block block : module.getBlocks()) {
                float x = (block.getX() + 1) * 32 * Constants.X_SCALE;
                float y = block.getY() * 32 * Constants.Y_SCALE;

                graphic.fillRect(x, y, 32 * Constants.X_SCALE, 32 * Constants.Y_SCALE);
            }
        }

        // Draw grid
        if (grid) {
            graphic.setColor(Color.red);

            for (int i = 1; i < 12; i++) {
                graphic.drawLine(32 * Constants.X_SCALE * i, 0, 32 * Constants.X_SCALE * i, (float) Constants.HEIGHT);
            }
            for (int i = 1; i < 21; i++) {
                graphic.drawLine(32 * Constants.X_SCALE, 32 * Constants.Y_SCALE * i, fieldX, 32 * Constants.Y_SCALE * i);
            }
        }

        // Draw preview
        graphic.setColor(backgroundColor);
        graphic.fillRect(fieldX + 20 * Constants.X_SCALE, 20 * Constants.Y_SCALE, 120 * Constants.X_SCALE, 72 * Constants.Y_SCALE);

        if (game.hasStarted() && game.getNext() != null) {
            Module next = game.getNext();

            graphic.setColor(next.getColor().getColor());

            int startX = next.getStructure().preViewPosition().x();
            int startY = next.getStructure().preViewPosition().y();
            int horizontalBlocks = next.getStructure().horizontalBlocks();

            for (Block block : next.getBlocks()) {
                float x = fieldX + 32 * Constants.X_SCALE + (startX + block.getRelativeX()) * 24 * Constants.X_SCALE;
                float y = 32 * Constants.Y_SCALE + (startY + block.getRelativeY()) * 24 * Constants.Y_SCALE;

                if (horizontalBlocks == 4) {
                    graphic.fillRect(x, y - 12 * Constants.Y_SCALE, 24 * Constants.X_SCALE, 24 * Constants.Y_SCALE);
                } else if (horizontalBlocks == 3) {
                    graphic.fillRect(x + 12 * Constants.X_SCALE, y, 24 * Constants.X_SCALE, 24 * Constants.Y_SCALE);
                } else {
                    graphic.fillRect(x, y, 24 * Constants.X_SCALE, 24 * Constants.Y_SCALE);
                }
            }
        }



        // Draw game start
        if (!game.hasStarted() && flash) {
            graphic.setFont(new TrueTypeFont(new Font("Calibri", 0, 20), false));
            graphic.setColor(Color.white);
            graphic.drawString("Press 'Space' to start the game", 64 * Constants.X_SCALE, 32 * Constants.Y_SCALE);
        }

        graphic.setColor(Color.black);
    }

    public void flash() {
        this.flash = !flash;
    }

    public void grid(boolean active) {
        this.grid = active;
    }
}
