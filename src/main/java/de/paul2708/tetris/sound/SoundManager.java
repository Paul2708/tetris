package de.paul2708.tetris.sound;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * Created by Paul on 25.07.2017.
 */
public class SoundManager {

    private Sound destroySound;

    private Music backGround;

    public SoundManager() {
        try {
            this.destroySound = new Sound("src/main/resources/sound/destroy.ogg");

            this.backGround = new Music("src/main/resources/music/background.ogg");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public void playMusic() {
        backGround.loop(1f, 0.15f);
    }

    public void destroy() {
        destroySound.play(1f, 1f);
    }
}
