package de.paul2708.tetris.state;

import de.paul2708.tetris.game.Field;
import de.paul2708.tetris.game.Game;
import de.paul2708.tetris.module.Direction;
import de.paul2708.tetris.module.Module;
import de.paul2708.tetris.util.Constants;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Paul on 16.07.2017.
 */
public class PlayState extends BasicGameState {

    private Game game;

    private Field field;

    public PlayState(Game game) {
        this.game = game;

        this.field = game.getField();
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        field.calculate();
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        game.getField().draw(graphics);
    }

    @Override
    public void update(GameContainer container, StateBasedGame stateBasedGame, int i) throws SlickException {
        Input input = container.getInput();

        if (!game.hasStarted()) {
            // Start game
            if (input.isKeyPressed(Input.KEY_SPACE)) {
                game.start();
            }
        } else {
            // Movement
            Module module = game.getCurrent();

            if (input.isKeyPressed(Input.KEY_A)) {
                if (module.canMove(Direction.LEFT) && !module.isStraight()) {
                    module.move(Direction.LEFT);
                }
            } else if (input.isKeyPressed(Input.KEY_D)) {
                if (module.canMove(Direction.RIGHT) && !module.isStraight()) {
                    module.move(Direction.RIGHT);
                }
            }

            // Fast movement
            module.straight(input.isKeyDown(Input.KEY_S));
        }

        // Grid
        game.getField().grid(input.isKeyDown(Input.KEY_TAB));
    }

    @Override
    public int getID() {
        return Constants.PLAY;
    }
}
