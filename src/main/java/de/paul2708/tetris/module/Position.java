package de.paul2708.tetris.module;

/**
 * Created by Paul on 26.07.2017.
 */
public class Position {

    private int x, y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position x(int x) {
        this.x = x;
        return this;
    }

    public Position y(int y) {
        this.y = y;
        return this;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }
}
