package de.paul2708.tetris.module;

/**
 * Created by Paul on 17.07.2017.
 */
public enum Direction {

    DOWN(0, 1),
    LEFT(-1, 0),
    RIGHT(1, 0),
    ;

    private int deltaX, deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }
}
