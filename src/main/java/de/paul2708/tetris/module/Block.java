package de.paul2708.tetris.module;

/**
 * Created by Paul on 17.07.2017.
 */
public class Block {

    private int x, y;
    private int relativeX, relativeY;

    public Block() {
        this.x = -1;
        this.y = -2;

        this.relativeX = -1;
        this.relativeY = -1;
    }

    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setRelative(int x, int y) {
        this.relativeX = x;
        this.relativeY = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRelativeX() {
        return relativeX;
    }

    public int getRelativeY() {
        return relativeY;
    }
}
