package de.paul2708.tetris.module;

import de.paul2708.tetris.game.Game;
import de.paul2708.tetris.module.structure.Structure;
import de.paul2708.tetris.util.Util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 16.07.2017.
 */
public class Module {

    private int id;
    private Structure structure;
    private Game game;

    private List<Block> blocks;
    private ModuleColor color;

    private boolean straight;

    public Module(int id, Structure structure, Game game) {
        this.id = id;
        this.structure = structure;
        this.game = game;

        this.blocks = new CopyOnWriteArrayList<>();
        this.color = ModuleColor.values()[Util.random.nextInt(ModuleColor.values().length)];

        this.straight = false;

        // Initialize blocks
        for (int i = 0; i < 4; i++) {
            blocks.add(new Block());
        }

        structure.apply(blocks);
    }

    public void spawn() {
        int startX = structure.spawnPosition().x();
        int startY = structure.spawnPosition().y();

        for (Block block : blocks) {
            block.setLocation(block.getRelativeX() + startX, block.getRelativeY() + startY);
        }
    }

    public void move(Direction direction) {
        for (Block block : blocks) {
            block.setLocation(block.getX() + direction.getDeltaX(), block.getY() + direction.getDeltaY());
        }
    }

    public boolean canMove(Direction direction) {
        for (Block block : blocks) {
            int x = block.getX() + direction.getDeltaX();
            int y = block.getY() + direction.getDeltaY();

            if (!isInBorder(x, y)) {
                return false;
            }
        }


        for (Module module : game.getModules()) {
            boolean collides = collides(module, direction);

            if (collides) {
                return false;
            }
        }

        return true;
    }

    private boolean isInBorder(int x, int y) {
        return x >= 0 && x < 10 && y >= 0 && y < 20;
    }

    private boolean collides(Module module, Direction direction) {
        if (this.getId() == module.getId()) {
            return false;
        }

        for (Block block : blocks) {
            int x = block.getX() + direction.getDeltaX();
            int y = block.getY() + direction.getDeltaY();

            for (Block other : module.getBlocks()) {
                if (x == other.getX() && y == other.getY()) {
                    return true;
                }
            }
        }

        return false;
    }

    public void straight(boolean straight) {
        this.straight = straight;
    }

    public void remove(Block block) {
        if (blocks.contains(block)) {
            blocks.remove(block);
        }
    }

    public int getLowest() {
        int y = blocks.get(0).getY();

        for (Block block : getBlocks()) {
            if (block.getY() > y) {
                y = block.getY();
            }
        }

        return y;
    }

    public boolean empty() {
        return blocks.size() == 0;
    }

    // Getter
    public int getId() {
        return id;
    }

    public Structure getStructure() {
        return structure;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public ModuleColor getColor() {
        return color;
    }

    public boolean isStraight() {
        return straight;
    }
}
