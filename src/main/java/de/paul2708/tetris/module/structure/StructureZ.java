package de.paul2708.tetris.module.structure;

import de.paul2708.tetris.module.Position;

/**
 * Created by Paul on 26.07.2017.
 */
public class StructureZ extends Structure {

    public StructureZ() {
        super("Z");
    }

    @Override
    public Position[] relativePositions() {
        Position[] array = new Position[4];

        array[0] = new Position(0, 0);
        array[1] = new Position(1, 0);
        array[2] = new Position(0, -1);
        array[3] = new Position(-1, -1);

        return array;
    }

    @Override
    public Position spawnPosition() {
        return new Position(4, 1);
    }

    @Override
    public Position preViewPosition() {
        return new Position(1, 1);
    }

    @Override
    public int horizontalBlocks() {
        return 3;
    }
}
