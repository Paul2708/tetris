package de.paul2708.tetris.module.structure;

import de.paul2708.tetris.module.Block;
import de.paul2708.tetris.module.Position;
import de.paul2708.tetris.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 26.07.2017.
 */
public abstract class Structure {

    private static List<Structure> structures = new ArrayList<>();

    private String type;

    public Structure(String type) {
        this.type = type;

        Structure.structures.add(this);
    }

    public abstract Position[] relativePositions();

    // Relates to origin 0, 0
    public abstract Position spawnPosition();

    public abstract Position preViewPosition();

    public void apply(List<Block> blocks) {
        Position[] array = relativePositions();

        for (int i = 0; i < blocks.size(); i++) {
            blocks.get(i).setRelative(array[i].x(), array[i].y());
        }
    }

    public abstract int horizontalBlocks();

    public String getType() {
        return type;
    }

    public static final Structure I = new StructureI();
    public static final Structure J = new StructureJ();
    public static final Structure L = new StructureL();
    public static final Structure O = new StructureO();
    public static final Structure S = new StructureS();
    public static final Structure T = new StructureT();
    public static final Structure Z = new StructureZ();

    public static Structure random() {
        return structures.get(Util.random.nextInt(structures.size()));
    }
}
