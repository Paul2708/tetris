package de.paul2708.tetris.module;

import org.newdawn.slick.Color;

/**
 * Created by Paul on 16.07.2017.
 */
public enum ModuleColor {

    WHITE(0, Color.white),
    RED(1, Color.red),
    GREEN(2, Color.green),
    YELLOW(3, Color.yellow),
    PINK(4, Color.pink),
    BLACK(5, Color.black),
    ;

    private int id;
    private Color color;

    ModuleColor(int id, Color color) {
        this.id = id;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

}
