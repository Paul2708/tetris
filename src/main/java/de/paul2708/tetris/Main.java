package de.paul2708.tetris;

import de.paul2708.tetris.game.Game;
import de.paul2708.tetris.util.Constants;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import java.io.File;

/**
 * Created by Paul on 13.07.2017.
 */
public class Main {

    public static void main(String[] args) {
        // Set the library path
        System.setProperty("org.lwjgl.librarypath", new File("").getAbsolutePath() + "/src/main/resources/natives");

        // Disable controllers
        Input.disableControllers();

        // Start application
        try {
            Game game = new Game(Constants.GAME_NAME);
            AppGameContainer app = new AppGameContainer(game);
            app.setDisplayMode((int) Constants.WIDTH, (int) Constants.HEIGHT, false);
            // app.setIcon("src/main/resources/icon.png");
            app.setShowFPS(false);
            app.setTitle(Constants.GAME_NAME);
            app.start();
        } catch(SlickException e) {
            e.printStackTrace();
        }
    }
}
