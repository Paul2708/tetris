package de.paul2708.tetris.util;

import java.awt.*;

/**
 * Created by Paul on 13.07.2017.
 */
public class Constants {

    public static final String GAME_NAME = "Tetris v0.1";

    // Auflösung: 1366 x 768
    public static final float X_SCALE = (float) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 1366;
    public static final float Y_SCALE = (float) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 768;

    public static final double WIDTH = 512 * X_SCALE; // 32 + 32 * 10 + 32 * 4
    public static final double HEIGHT = 640 * Y_SCALE; // 32 * 20

    // States
    public static final int	PLAY = 0;
}
